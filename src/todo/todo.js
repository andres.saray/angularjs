angular
  .module("firstApp", ["LocalStorageModule", "angular-uuid"])
  .controller("firstController", ($scope, localStorageService, uuid) => {
    $scope.todos = [];
    if (localStorageService.get("angularTodos")) {
      $scope.todos = localStorageService.get("angularTodos");
    }
    $scope.newTodo = {
      name: "",
      time: "",
    };

    $scope.$watch(() =>$scope.newTodo, (newValue, oldValue) => {
        console.log('newValue', newValue)
        console.log('oldValue', oldValue)
    })
    $scope.$watchCollection(() =>$scope.todos, (newValue, oldValue) => {
        localStorageService.set("angularTodos", $scope.todos);
    })
    $scope.addTodo = () => {
      $scope.todos.push({
        uuid: uuid.v4(),
        ...$scope.newTodo,
      });
      
      $scope.newTodo = {
        name: "",
        time: "",
      };
    };

    $scope.deleteTodo = (uuidTodo = "") => {
      $scope.todos = $scope.todos.filter((todo) => todo.uuid !== uuidTodo);
    };
  });
