angular.module("firstApp", []).controller("firstController", ($scope, $http) => {
    $scope.posts = [];
    $http.get("http://jsonplaceholder.typicode.com/posts").then((response) => {
        $scope.posts = response;
    }).catch((r) => {
        console.log('r', r)
    })

});