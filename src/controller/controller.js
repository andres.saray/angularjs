const app = angular.module("firstApp", []);
app.controller("firstController", ($scope) => {
    $scope.name = "Andres";
    $scope.newComment = {
        name: "",
        comment: ""
    };
    $scope.comments = [
        {
            name: "Andres",
            comment: "No seas loca"
        },
        {
            name: "Julian",
            comment: "No seas locota"
        },
        {
            name: "Carlos",
            comment: "No seas burro"
        }
    ]
    $scope.addComment = () =>{
        $scope.comments.push($scope.newComment);
        $scope.newComment = {
            name: "",
            comment: ""
        };
    }
});